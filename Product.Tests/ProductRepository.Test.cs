using System;
using ProductNamespace;
using Product;
using Xunit;
using System.Diagnostics;
using FluentAssertions;

namespace Product.Tests
{
    public class Product
    {
        [Fact]
        public void RetrieveActiveProductsShouldReturnProductsWhereDateIsValid()
        {

            // Arrange
            var p1 = new ProductNamespace.Product(1,"ddd","desc",new DateTime(2015, 1, 3, 5, 2, 1), new DateTime(2019, 1, 3, 5, 2, 1),2, 3);
            ProductNamespace.Product p2=new ProductNamespace.Product( 2, "dd", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.Product p3=new ProductNamespace.Product( 2, "D", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.ProductRepository products = new ProductNamespace.ProductRepository(p1,p2,p3);

            var prods= products.RetriveActiveProducts;
        
        foreach(ProductNamespace.Product product in  prods) {
        // Act
            var validation = product.IsValid();

        // Assert
            validation.Should().Be(true);
        }
        }

        [Fact]
        public void RetrieveInactiveProductsShouldReturnProductsWhereDateIsNotValid()
        {

            // Arrange
            var p1 = new ProductNamespace.Product(1,"ddd","desc",new DateTime(2015, 1, 3, 5, 2, 1), new DateTime(2019, 1, 3, 5, 2, 1),2, 3);
            ProductNamespace.Product p2=new ProductNamespace.Product( 2, "dd", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.Product p3=new ProductNamespace.Product( 2, "D", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.ProductRepository products = new ProductNamespace.ProductRepository(p1,p2,p3);

            var prods= products.RetriveActiveProducts;
        
        foreach(ProductNamespace.Product product in  prods) {
        // Act
            var validation = product.IsValid();

        // Assert
            validation.Should().Be(false);
        }
        }

        public void RetrieveAllShouldReturnListAllProducts()
        {

            // Arrange
            var p1 = new ProductNamespace.Product(1,"ddd","desc",new DateTime(2015, 1, 3, 5, 2, 1), new DateTime(2019, 1, 3, 5, 2, 1),2, 3);
            ProductNamespace.Product p2=new ProductNamespace.Product( 2, "dd", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.Product p3=new ProductNamespace.Product( 2, "D", "desc", DateTime.Today.AddDays(-7), DateTime.Today.AddDays(5), 20, 35);
            ProductNamespace.ProductRepository products = new ProductNamespace.ProductRepository(p1,p2,p3);
            var result= products.RetriveAll;
            List<Product> p = new List();
            p.Append(p1);p.Append(p2);p.Append(p3);
            // Act

           result = (result == p);

            // Assert 
         result.Shoul().Be(True);
        }
    }
    }

