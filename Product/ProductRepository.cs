using System;
using System.Collections.Generic;
using System.Linq;
using ProductNamespace;

// methods with query
namespace ProductNamespace
{
    public class ProductRepository
    {       
        public static List<Product> _products {get {return _products;} set {_products = value;}}

        public ProductRepository(params Product[] products) 
        {
            _products = new List<Product>();
            for(int i = 0; i < products.Length; i++) 
            { 
                CheckProductForNullAndValid(products[i]);
                _products.Add(products[i]);
            }
        }

        public Product GetProductByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new System.ArgumentException();
            }

            for(int i = 0; i < _products.Count; i++)
            {
                if(_products[i].GetProductName().CompareTo(name) == 0)
                {
                    return _products[i];
                }
            }
            return null;
        }

        public void AddProduct(Product product) 
        {
            CheckProductForNullAndValid(product);
            _products.Add(product);
        }

        public Product GetProductByPosition(int position)
        {
            if(position < 0 || _products.Count < position - 1)
                throw new System.ArgumentException("Position has to be >= 0 and < amount of products", "position");
            return _products[position];
        }

        public List<Product> FindAllProducts()
        {
            return _products;
        }

        public void RemoveProductByName(string name) 
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new System.ArgumentException("Parameter cannot null or empty", "name");
            }

            for(int i = 0; i < _products.Count; i++)
            {
                if(_products[i].GetProductName().CompareTo(name) == 0)
                {
                    _products.RemoveAt(i);
                    i--;
                }
            }

        }

        private void CheckProductForNullAndValid(Product product)
        {
            if(product != null && product.IsValid() == false)
            {
                throw new System.ArgumentException("Product has to be valid and not null", "product");
            }
        }

        public IEnumerable<Product> RetriveActiveProducts = from product in _products
                                                     where product.IsValid() == true
                                                     select product;

        IEnumerable<Product> RetriveInactiveProducts = from product in _products
                                                     where product.IsValid() == false
                                                     select product;

        IEnumerable<Product> RetriveAllOrderByPriceDescending = from product in _products
                                                     orderby product.Price descending
                                                     select product;

        IEnumerable<Product> RetriveAllOrderByPriceAscending = from product in _products
                                                     orderby product.Price ascending
                                                     select product;

        IEnumerable<Product> RetriveAll = from product in _products
                                                     orderby product.Price ascending
                                                     select product;
    }
}

