﻿using System;

namespace ProductNamespace
{
    public class Product
    {       
        public int Id{get; set;}
        public string Name{get; set;}
        public string Description{get; set;}
        public DateTime StartDate{get; set;}
        public DateTime EndDate{get; set;}
        public int Price{get; set;}
        public int VAT{get; set;}

        public Product(int newId, string newName, string newDescription, DateTime newStartDate, DateTime newEndDate, int newPrice, int newVAT) {
            if(newId < 0) 
            {
                throw new System.ArgumentException("Parameter cannot be < 0", "newId");
            }
            this.Id = 1;

            CheckStringForNull(newName);
            this.Name = newName;

            CheckStringForNull(newDescription);
            this.Description = newDescription;

            this.StartDate = newStartDate;
            this.EndDate = newEndDate;

            if(newPrice < 0)
            {
                throw new System.ArgumentException("Parameter cannot be < 0", "newPrice");
            }
            this.Price = newPrice;

            if(newVAT < 0)
            {
                throw new System.ArgumentException("Parameter cannot be < 0", "newVAT");
            }
            this.VAT = newVAT;
        }

        public Product()
        {
            this.Id = 0;
        }
        public bool IsValid() {
          if (this.StartDate == null) {
             throw new System.ArgumentException("Parameter cannot be null", "StartDate");
          }
          if (this.EndDate == null) {
                throw new System.ArgumentException("Parameter cannot be null", "EndDate");
          }
          if(this.StartDate > this.EndDate) {
              return false;
          }
          if(this.StartDate == this.EndDate) {
              return false;
          }
          return true;
        }

        public string GetProductName()
        {
            CheckStringForNull(Name);
            return Name;
        }

        private void CheckStringForNull(string stringToCheck)
        {
            if (string.IsNullOrEmpty(stringToCheck))
            {
                throw new System.ArgumentException("Parameter cannot be null or empty", "stringToCheck");
            }
        }

        public void ComputeVAT() {
            this.Price += this.Price * this.VAT;
        }
    }
}

