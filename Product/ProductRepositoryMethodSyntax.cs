using System;
using System.Collections.Generic;
using System.Linq;
using ProductNamespace;

// methods with query
namespace ProductNamespace
{
    public class ProductRepositoryMethod
    {       
        public static List<Product> _products {get {return _products;} set {_products = value;}}

        // public ProductdRepositoryMethod( Product[] products) 
        // {
        //     _products = new List<Product>();
        //     for(int i = 0; i < products.Length; i++) 
        //     { 
        //         CheckProductForNullAndValid(products[i]);
        //         _products.Add(products[i]);
        //     }
        // }

       public  ProductRepositoryMethod(params Product[] products){
            _products = new List<Product>();
            for(int i = 0; i < products.Length; i++) 
            { 
                CheckProductForNullAndValid(products[i]);
                _products.Add(products[i]);
            }
        }

        public Product GetProductByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new System.ArgumentException();
            }

            for(int i = 0; i < _products.Count; i++)
            {
                if(_products[i].GetProductName().CompareTo(name) == 0)
                {
                    return _products[i];
                }
            }
            return null;
        }

        public void AddProduct(Product product) 
        {
            CheckProductForNullAndValid(product);
            _products.Add(product);
        }

        public Product GetProductByPosition(int position)
        {
            if(position < 0 || _products.Count < position - 1)
                throw new System.ArgumentException("Position has to be >= 0 and < amount of products", "position");
            return _products[position];
        }

        public List<Product> FindAllProducts()
        {
            return _products;
        }

        public void RemeoveProductByName(string name) 
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new System.ArgumentException("Parameter cannot null or empty", "name");
            }

            for(int i = 0; i < _products.Count; i++)
            {
                if(_products[i].GetProductName().CompareTo(name) == 0)
                {
                    _products.RemoveAt(i);
                    i--;
                }
            }

        }

        private void CheckProductForNullAndValid(Product product)
        {
            if(product != null && product.IsValid() == false)
            {
                throw new System.ArgumentException("Product has to be valid and not null", "product");
            }
        }

        public IEnumerable<Product> RetriveActiveProducts(){

            if (_products == null){
                throw new ArgumentNullException("_products is null");
            }

            IEnumerable<Product> list = new List<Product>();
            foreach( Product prod in _products ){
                    if (prod.IsValid()){
                        list.Append(prod);
                    }
            }
            return list;
        }

        IEnumerable<Product> RetriveInactiveProducts(){

            if (_products == null){
                throw new System.ArgumentException("Parameter cannot be null", "original");
   
            }

            IEnumerable<Product> list = new List<Product>();
            foreach( Product prod in _products ){
                    if (prod.IsValid()==false){
                        list.Append(prod);
                    }
            }

            return list;
        }

        // IEnumerable<Product> RetriveAllOrderByPriceDescending(){
        //     IEnumerable<Product> list = new List<Product>();
        // }

        IEnumerable<Product> RetriveAllOrderByPriceAscending (){
            IEnumerable<Product> orderedList = new List<Product>();
            orderedList = _products.OrderBy(o=>o.Price).ToList();

            return orderedList;
        }

        IEnumerable<Product> RetriveAll(){
            if(_products == null){
                throw new ArgumentNullException("_products");
            }
            return _products;
        }
    }
}

